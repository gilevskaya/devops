import * as moment from "moment";

export function dateToString(d: Date) {
  return moment(d).format("MMMM Do YYYY, h:mm:ss");
}
